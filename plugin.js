// TODO make this configurable
var PBLIVE_URI = "ws://localhost:2081";

var decksocket = null;
var pbsocket = null;
var pluginUUID = null;

var DestinationEnum = Object.freeze({
  HARDWARE_AND_SOFTWARE: 0,
  HARDWARE_ONLY: 1,
  SOFTWARE_ONLY: 2,
});

var timer;

var pbAction = {
  type: "com.panterasbox.pblive.action",
  pbsocket: undefined,

  onKeyDown: function (context, settings, coordinates, userDesiredState) {
    timer = setTimeout(() => {
      this.pbSend(context, "reset", { "settings" : settings });
    }, 1500);
  },

  onKeyUp: function (context, settings, coordinates, userDesiredState) {
    var reset = true;
    if (timer) {
      clearTimeout(timer);
      reset = false;
    }
    if (!reset) {
      this.pbSend(context, "execute", { "settings" : settings });
    }
  },

  onWillAppear: function (context, settings, coordinates) {
    // this.settings[context] = settings;
    this.pbSend(context, "init", { "settings": settings });
  },

  onApplicationDidLaunch: function(context, application) {
    this.pbSend(context, "appLaunched", {
      "application": application
    });
  },

  onApplicationDidTerminate: function(context, application) {
    this.pbSend(context, "appTerminated", {
      "application": application
    });
  },

  onSendToPlugin: function(context, action, jsonPayload) {
    // console.log("onSendToPlugin", action, jsonPayload);
    // // if ('uri' in jsonPayload) {
    // // }
    // if ('function' in jsonPayload) {
    //   this.settings[context].config.function = jsonPayload.function;
    // }

    // var json = {
    //   event: "setSettings",
    //   context: context,
    //   payload: this.settings[context],
    // };
    // decksocket.send(JSON.stringify(json));

    // // this.openPbSocket(context, this.settings);
  },

  didReceiveSettings: function(context, settings) {
    // if (settings.config.uri != this.settings.config.uri) {
    //   this.openPbSocket(context, settings);
    // }
    // this.settings[context] = settings;
  },

  openPbSocket() {
    if (this.pbsocket && (this.pbsocket.readyState != WebSocket.CLOSED)) {
      this.pbsocket.close();
    }
    // this.pbsocket = new WebSocket(settings.config.uri);
    this.pbsocket = new WebSocket(PBLIVE_URI);

    this.pbsocket.onopen = () => {
      console.log("Connected to live server on " + PBLIVE_URI);
    };
  
    this.pbsocket.onmessage = function (evt) {
      console.log("got command", decksocket, evt);
      decksocket.send(evt.data);
    };

    this.pbsocket.onclose = function () {
    };
  },

  pbSend(context, action, payload) {
    if (this.pbsocket.readyState != WebSocket.OPEN) {
      decksocket.send(JSON.stringify({
        "event": "showAlert",
        "context": context,
      }));
    } else {
      console.log("pbSend", action, payload);
      this.pbsocket.send(JSON.stringify({
        "context": context,
        "action": action,
        "payload": payload
      }));
    }
  },

};

function connectElgatoStreamDeckSocket(
  inPort,
  inPluginUUID,
  inRegisterEvent,
  inInfo
) {
  pluginUUID = inPluginUUID;

  // Open the web socket
  decksocket = new WebSocket("ws://127.0.0.1:" + inPort);
  pbAction.openPbSocket();

  function registerPlugin(inPluginUUID) {
    var json = {
      event: inRegisterEvent,
      uuid: inPluginUUID,
    };

    decksocket.send(JSON.stringify(json));
  }

  decksocket.onopen = function () {
    // WebSocket is connected, send message
    registerPlugin(pluginUUID);
  };

  decksocket.onmessage = function (evt) {
    // Received message from Stream Deck
    var jsonObj = JSON.parse(evt.data);
    var event = jsonObj["event"];
    var action = jsonObj["action"];
    var context = jsonObj["context"];

    if (event == "keyDown") {
      var jsonPayload = jsonObj["payload"];
      var settings = jsonPayload["settings"];
      var coordinates = jsonPayload["coordinates"];
      var userDesiredState = jsonPayload["userDesiredState"];
      pbAction.onKeyDown(context, settings, coordinates, userDesiredState);
    } else if (event == "keyUp") {
      var jsonPayload = jsonObj["payload"];
      var settings = jsonPayload["settings"];
      var coordinates = jsonPayload["coordinates"];
      var userDesiredState = jsonPayload["userDesiredState"];
      pbAction.onKeyUp(context, settings, coordinates, userDesiredState);
    } else if (event == "willAppear") {
      var jsonPayload = jsonObj["payload"];
      var settings = jsonPayload["settings"];
      var coordinates = jsonPayload["coordinates"];
      pbAction.onWillAppear(context, settings, coordinates);
    } else if (event == "applicationDidLaunch") {
      var jsonPayload = jsonObj["payload"];
      var application = jsonPayload["application"];
      pbAction.onApplicationDidLaunch(context, application);
    } else if (event == "applicationDidTerminate") {
      var jsonPayload = jsonObj["payload"];
      var application = jsonPayload["application"];
      pbAction.onApplicationDidTerminate(context, application);
    } else if (event == "sendToPlugin") {
      var jsonPayload = jsonObj["payload"];
      pbAction.onSendToPlugin(context, action, jsonPayload);
    } else if (event == "didReceiveSettings") {
      var jsonPayload = jsonObj["payload"];
      var settings = jsonPayload["settings"];
      pbAction.didReceiveSettings(context, settings);
    }
  };

  decksocket.onclose = function () {
    // Websocket is closed
  };
}
